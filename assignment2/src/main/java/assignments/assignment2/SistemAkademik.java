package assignments.assignment2;

import java.util.Arrays;
import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        // mencari mahasiswa berdasarkan NPM
        for (Mahasiswa mhs : daftarMahasiswa) {
            if (mhs.getNPM() == npm){
                return mhs;
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        // mencari mata kuliah berdasarkan nama
        for (MataKuliah matkul : daftarMataKuliah) {
            if (matkul.getNama().equals(namaMataKuliah)) {
                return matkul;
            }
        }
        return null;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        Mahasiswa mhs = getMahasiswa(npm);

        // menambahkan mata kuliah yang diambil seorang mahasiswa
        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");

        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            mhs.addMatkul(mataKuliah);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mhs = getMahasiswa(npm);

        // handle jika belum ada matkul diambil
        boolean dontHaveMatkul = true;
        for (MataKuliah matkul : mhs.getListMataKuliahMhs()) {
            if (matkul != null) {
                dontHaveMatkul = false;
            }
        }

        if (dontHaveMatkul) {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        }
   
        // menghapus mata kuliah seorang mahasiswa
        else {
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
                mhs.dropMatkul(mataKuliah);

            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());

        // mencetak informasi mahasiswa
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + getMahasiswa(npm).getNama());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + getMahasiswa(npm).getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        // handle jika belum ada mata kuliah diambil
        boolean hasMatkul = false;
        for (MataKuliah matkuls : getMahasiswa(npm).getListMataKuliahMhs()) {
            if (matkuls != null) {
                hasMatkul = true;
                break;
            }
        }

        // mencetak matkul
        if (hasMatkul) {
            int k = 1;
            for (MataKuliah matkuls : getMahasiswa(npm).getListMataKuliahMhs()) {
                if (matkuls != null){
                    System.out.println(k + ". " + matkuls);
                }
                k ++;
            }
        }

        else {
            System.out.println("Belum ada mata kuliah yang diambil");
        }
        
        System.out.println("Total SKS: " + getMahasiswa(npm).getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");
        
        getMahasiswa(npm).cekIRS();
       
        // handle jika IRS tidak bermasalah
        boolean hasProblem = false;
        for (String probs : getMahasiswa(npm).getMasalahIRS()) {
            if (probs != null) {
                hasProblem = true;
                break;
            }
        }

        // cetak masalah IRS
        if (hasProblem) {
            int j = 1;
            for (String probs : getMahasiswa(npm).getMasalahIRS()) {
                if (probs != null) {
                    System.out.println(j + ". " + probs);
                }
                j ++;
            }
        }
        else {
            System.out.println("IRS tidak bermasalah.");
        }
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        
        // mencetak informasi mata kuliah
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + getMataKuliah(namaMataKuliah).getNama());
        System.out.println("Kode: " + getMataKuliah(namaMataKuliah).getKode());
        System.out.println("SKS: " + getMataKuliah(namaMataKuliah).getSKS());
        System.out.println("Jumlah mahasiswa: " + getMataKuliah(namaMataKuliah).getJumlahMahasiswa());
        System.out.println("Kapasitas: " + getMataKuliah(namaMataKuliah).getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       
        // handle jika belum ada mahasiswa yang mengambil
        boolean hasMahasiswa = false;
        for (Mahasiswa mhs : getMataKuliah(namaMataKuliah).getDaftarMahasiswa()) {
            if (mhs != null) {
                hasMahasiswa = true;
                break;
            }
        }

        if (hasMahasiswa) {
            int l = 1;
            for (Mahasiswa mhs : getMataKuliah(namaMataKuliah).getDaftarMahasiswa()) {
                if (mhs != null) {
                    System.out.println(l + ". " + mhs);
                }
                l ++;
            }
        }
        else {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        // meminta input data matkul dan memasukkannya ke dalam array
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            MataKuliah matKul = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
            daftarMataKuliah[i] = matKul;
        }

        // meminta input data matkul dan memasukkannya ke dalam array
        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");
        
        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            Mahasiswa mhs = new Mahasiswa(dataMahasiswa[0], npm);
            daftarMahasiswa[i] = mhs;
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
