package assignments.assignment2;

import java.util.Arrays;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    
    // constructor
    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
    }
    
    // getter
    public long getNPM() {
        return this.npm;
    }
    
    public String getNama() {
        return this.nama;
    }
    
    public String getJurusan() {
        return setJurusan();
    }
    
    public int getTotalSKS() {
        return setTotalSKS();
    }

    public MataKuliah[] getListMataKuliahMhs() {
        return this.mataKuliah;
    }

    public String[] getMasalahIRS() {
        return this.masalahIRS;
    }
    
    // setter
    public String setJurusan() {
        String npmStr = Long.toString(this.npm);
        if (npmStr.substring(2,4).equals("01")) {
            this.jurusan = "Ilmu Komputer";
        }
        else if (npmStr.substring(2,4).equals("02")) {
            this.jurusan = "Sistem Informasi";
        }
        else {
        }
        return this.jurusan;
    }
    
    public int setTotalSKS() {
        this.totalSKS = 0;
        for (MataKuliah matkul : getListMataKuliahMhs()) {
            if (matkul != null) {
                this.totalSKS += matkul.getSKS();
            }
        }
        return this.totalSKS;
    }

    
    public void addMatkul(MataKuliah mataKuliah){
        // handle jika matkul sudah diambil
        boolean sudahAda = false;
        for (int i = 0; i < 10; i++) {
            if (this.mataKuliah[i] == mataKuliah) {
                System.out.println("[DITOLAK] " + mataKuliah +  " telah diambil sebelumnya.");
                sudahAda = true;
                break;
            }
            else {
                continue;
            } 
        }

        if (!sudahAda) {
            for (int i = 0; i < 10; i++) {
                // handle kapasitas dan jumlah matkul yang diambil
                if (!mataKuliah.cekKapasitasMatkul()) {
                    System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah penuh kapasitasnya.");
                    break;  
                }
    
                else if (this.mataKuliah[i] != null && i == 9) {
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                    break;
                }
                
                // menambah mata kuliah ke array mataKuliah
                else if (this.mataKuliah[i] == null) {
                    this.mataKuliah[i] = mataKuliah;
                    mataKuliah.addMahasiswa(this);
                    break;
                }
    
                else {
                    continue;
                }

            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        boolean ada = false;
        int indeks = 0;
        for (int i = 0; i < 10; i++) {
            if (this.mataKuliah[i] == mataKuliah) {
                ada = true;
                indeks = i;
                break;
            }
        }

        // menghapus mata kuliah dari array mataKuliah
        if (ada) {
            mataKuliah.dropMahasiswa(this);
            for (int j = indeks; j < 10; j++) {
                if (j == 9) {
                    this.mataKuliah[j] = null;
                }
                else {
                    this.mataKuliah[j] = this.mataKuliah[j + 1];
                }
            }
        }

        // handle jika mata kuliah belum pernah diambil
        else {
            System.out.println("[DITOLAK] " + mataKuliah + " belum pernah diambil.");
        }
    }


    public void cekIRS(){
        masalahIRS = new String[20];
        int i = 0;
        
        // menambahkan masalah ke array masalahIRS
        if (getTotalSKS() > 24) {
            this.masalahIRS[i] = "SKS yang Anda ambil lebih dari 24";
            i = 1;
        }

        for (MataKuliah matkul : this.mataKuliah) {
            if (matkul != null) {            
                if ((!matkul.cekKodeMatkul(this.getJurusan())) && this.getJurusan().equals("Ilmu Komputer")) {
                    this.masalahIRS[i] = "Mata Kuliah " + matkul.getNama() + " tidak dapat diambil jurusan " + "IK";
                    i++;
                }
                else if ((!matkul.cekKodeMatkul(this.getJurusan())) && this.getJurusan().equals("Sistem Informasi")) {
                    this.masalahIRS[i] = "Mata Kuliah " + matkul.getNama() + " tidak dapat diambil jurusan " + "SI";
                    i++;  
                }
                else {
                    continue;
                }
            }
        }
    }

    public String toString() {
        return this.nama;
    }

}
