package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;

    // constuctor
    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }
    
    // getter
    public String getNama() {
        return this.nama;
    }
    
    public int getSKS() {
        return this.sks;
    }
    
    public String getKode() {
        return this.kode;
    }
    
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }
    
    public int getJumlahMahasiswa() {
        int sum = 0;
        for (int j = 0; j < this.kapasitas; j++) {
            if (this.daftarMahasiswa[j] != null) {
                sum += 1;
            }
            else {
                continue;
            }
        }
        return sum;
    }
    
    public int getKapasitas() {
        return this.kapasitas;
    }
    
    public void addMahasiswa(Mahasiswa mahasiswa) {
        // menambahkan mahasiswa ke daftarMahasiswa
        for (int i = 0; i < this.kapasitas; i++) {
            if (this.daftarMahasiswa[i] == null) {
                this.daftarMahasiswa[i] = mahasiswa;
                break;
            }
        }
    }
    
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int indeks = 0;
        for (int i = 0; i < kapasitas; i++) {
            if (this.daftarMahasiswa[i] == mahasiswa) {
                indeks = i;
                break;
            }
        }

        // menghapus mahasiswa dari array
        for (int j = indeks; j < kapasitas; j++) {
            if (j == kapasitas-1) {
                this.daftarMahasiswa[j] = null;
            }
            else {
                this.daftarMahasiswa[j] = this.daftarMahasiswa[j + 1];
            }
        }
    
    }
    
    // method chekcer untuk membantu validasi kapasitas
    public boolean cekKapasitasMatkul() {
        boolean notFull = true;
        for (int j = 0; j < this.kapasitas; j++) {
            if (this.daftarMahasiswa[j] == null) {
                notFull = true;
            }
            else {
                notFull = false;
            }
        }
        return notFull;
    }

    // method chekcer untuk membantu validasi kesesuaian jurusan
    public boolean cekKodeMatkul(String jurusan) {
        if (jurusan.equals("Ilmu Komputer") && this.kode.equals("IK")) {
            return true;
        }
        else if (jurusan.equals("Sistem Informasi") && this.kode.equals("SI")) {
            return true;
        }
        else if (this.kode.equals("CS")) {
            return true;
        }
        else {
            return false;
        }
    }

    public String toString() {
        return this.nama;
    }
}
