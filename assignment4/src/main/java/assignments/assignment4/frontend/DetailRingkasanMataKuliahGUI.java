package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Setting panel
        JPanel detailRingkasanMataKuliahPanel = new JPanel();
        detailRingkasanMataKuliahPanel.setLayout(new BoxLayout(detailRingkasanMataKuliahPanel, BoxLayout.Y_AXIS));
        detailRingkasanMataKuliahPanel.setBackground(new Color(30,40,60,255));

        // Setting judul
        JLabel titleLabel = new JLabel("Detail Ringkasan Mata Kuliah");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);

        // Setting data
        JLabel nama = new JLabel(String.format("Nama: %s", mataKuliah.getNama()));
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setFont(SistemAkademikGUI.fontGeneral);
        nama.setForeground(Color.white);
        
        JLabel kode = new JLabel(String.format("Kode: %s", mataKuliah.getKode()));
        kode.setAlignmentX(Component.CENTER_ALIGNMENT);
        kode.setFont(SistemAkademikGUI.fontGeneral);
        kode.setForeground(Color.white);
        
        JLabel sks = new JLabel(String.format("SKS: %s", mataKuliah.getSKS()));
        sks.setAlignmentX(Component.CENTER_ALIGNMENT);
        sks.setFont(SistemAkademikGUI.fontGeneral);
        sks.setForeground(Color.white);
        
        JLabel jumlah = new JLabel(String.format("Jumlah mahasiswa: %s", mataKuliah.getJumlahMahasiswa()));
        jumlah.setAlignmentX(Component.CENTER_ALIGNMENT);
        jumlah.setFont(SistemAkademikGUI.fontGeneral);
        jumlah.setForeground(Color.white);
        
        JLabel kapasitas = new JLabel(String.format("Kapasitas: %s", mataKuliah.getKapasitas()));
        kapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitas.setFont(SistemAkademikGUI.fontGeneral);
        kapasitas.setForeground(Color.white);
        
        JLabel mahasiswa = new JLabel("Daftar Mahasiswa:");
        mahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        mahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        mahasiswa.setForeground(Color.white);
        
        JPanel listMhs = new JPanel();
        listMhs.setBackground(new Color(30,40,60,255));
        listMhs.setLayout(new BoxLayout(listMhs, BoxLayout.Y_AXIS));
        JLabel mhz;
        if (mataKuliah.getJumlahMahasiswa() != 0) {
            for (int k = 0; k < mataKuliah.getJumlahMahasiswa(); k++) {
                mhz = new JLabel(String.format("%s. %s\n", k+1, mataKuliah.getDaftarMahasiswa()[k].getNama()));
                mhz.setAlignmentX(Component.CENTER_ALIGNMENT);
                mhz.setForeground(Color.white);
                listMhs.add(mhz);
                listMhs.add(Box.createRigidArea(new Dimension(0, 10)));
            }
        }
        else {
            mhz = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            mhz.setAlignmentX(Component.CENTER_ALIGNMENT);
            mhz.setForeground(Color.white);
            listMhs.add(mhz);
        }

        // Setting button
        JButton selesaiButton = new JButton("Selesai");
        selesaiButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        selesaiButton.setBackground(new Color(190,170,140,255));
        selesaiButton.setForeground(Color.black);
        selesaiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                detailRingkasanMataKuliahPanel.setVisible(false);
            }
        });
        
        // Menempelkan komponen ke panel
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 70)));
        detailRingkasanMataKuliahPanel.add(titleLabel);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        detailRingkasanMataKuliahPanel.add(nama);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMataKuliahPanel.add(kode);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMataKuliahPanel.add(sks);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMataKuliahPanel.add(jumlah);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMataKuliahPanel.add(kapasitas);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMataKuliahPanel.add(mahasiswa);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMataKuliahPanel.add(listMhs);
        detailRingkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        detailRingkasanMataKuliahPanel.add(selesaiButton);

        // Menempelkan panel ke frame
        frame.add(detailRingkasanMataKuliahPanel);
    }
}
