package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Setting panel
        JPanel ringkasanMataKuliahPanel = new JPanel();
        ringkasanMataKuliahPanel.setLayout(new BoxLayout(ringkasanMataKuliahPanel, BoxLayout.Y_AXIS));
        ringkasanMataKuliahPanel.setBackground(new Color(84,16,29,255));

        JLabel titleLabel = new JLabel("Ringkasan Mata Kuliah");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);

        // Setting dropdown
        String[] daftarNamaMatkul = new String[daftarMataKuliah.size()];
        int j = 0;
        for (MataKuliah i : daftarMataKuliah) {
            daftarNamaMatkul[j++] = i.getNama(); 
        }

        for (int m = 0; m < daftarNamaMatkul.length; m++) {
            for (int n = m+1; n < daftarNamaMatkul.length; n++) {
                String tmp = "";
                if (daftarNamaMatkul[m].compareTo(daftarNamaMatkul[n]) > 0) {
                    tmp = daftarNamaMatkul[m];
                    daftarNamaMatkul[m] = daftarNamaMatkul[n];
                    daftarNamaMatkul[n] = tmp;
                }
            }
        }

        JLabel pilihNamaLabel = new JLabel("Pilih Nama Matkul");
        pilihNamaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihNamaLabel.setForeground(Color.white);
        JComboBox<String> pilihNama = new JComboBox<String>(daftarNamaMatkul);
        pilihNama.setMaximumSize(new Dimension(200,23));
        
        // Setting button
        JButton lihatButton = new JButton("Lihat");
        lihatButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatButton.setBackground(new Color(130,170,190,255));
        lihatButton.setForeground(Color.black);
        lihatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                try {
                    MataKuliah matakuliah = getMataKuliah((String)pilihNama.getSelectedItem(), daftarMataKuliah);
                    new DetailRingkasanMataKuliahGUI(frame, matakuliah, daftarMahasiswa, daftarMataKuliah);
                    ringkasanMataKuliahPanel.setVisible(false);
                }
                catch (Exception f) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh field", "Peringatan", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(new Color(190,170,140,255));
        kembaliButton.setForeground(Color.black);
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                ringkasanMataKuliahPanel.setVisible(false);
            }
        });

        ringkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0,145)));
        ringkasanMataKuliahPanel.add(titleLabel);
        ringkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0,20)));
        ringkasanMataKuliahPanel.add(pilihNamaLabel);
        ringkasanMataKuliahPanel.add(pilihNama);
        ringkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0,20)));
        ringkasanMataKuliahPanel.add(lihatButton);
        ringkasanMataKuliahPanel.add(Box.createRigidArea(new Dimension(0,10)));
        ringkasanMataKuliahPanel.add(kembaliButton);


        frame.add(ringkasanMataKuliahPanel);
        
    }

    // Uncomment method di bawah jika diperlukan
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

}
