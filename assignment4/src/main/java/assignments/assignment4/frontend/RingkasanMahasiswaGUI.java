package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Setting panel
        JPanel ringkasanMahasiswaPanel = new JPanel();
        ringkasanMahasiswaPanel.setLayout(new BoxLayout(ringkasanMahasiswaPanel, BoxLayout.Y_AXIS));
        ringkasanMahasiswaPanel.setBackground(new Color(84,16,29,255));

        // Setting judul
        JLabel titleLabel = new JLabel("Ringkasan Mahasiswa");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);

        // Setting dropdown
        Long[] daftarNPMmahasiswa = new Long[daftarMahasiswa.size()];
        int j = 0;
        for (Mahasiswa i : daftarMahasiswa) {
            daftarNPMmahasiswa[j++] = i.getNpm(); 
        }

        for (int m = 0; m < daftarNPMmahasiswa.length; m++) {
            for (int n = m+1; n < daftarNPMmahasiswa.length; n++) {
                long tmp = 0;
                if (daftarNPMmahasiswa[m] > daftarNPMmahasiswa[n]) {
                    tmp = daftarNPMmahasiswa[m];
                    daftarNPMmahasiswa[m] = daftarNPMmahasiswa[n];
                    daftarNPMmahasiswa[n] = tmp;
                }
            }
        }

        JLabel pilihNPMLabel = new JLabel("Pilih NPM");
        pilihNPMLabel.setForeground(Color.white);
        pilihNPMLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        JComboBox<Long> pilihNPM = new JComboBox<Long>(daftarNPMmahasiswa);
        pilihNPM.setMaximumSize(new Dimension(200,23));
        
        // Setting button
        JButton lihatButton = new JButton("Lihat");
        lihatButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatButton.setBackground(new Color(130,170,190,255));
        lihatButton.setForeground(Color.black);
        lihatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // try {
                    Mahasiswa mahasiswa = getMahasiswa((long)pilihNPM.getSelectedItem(), daftarMahasiswa);
                    new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                    ringkasanMahasiswaPanel.setVisible(false);
                // } catch (Exception f) {
                //     JOptionPane.showMessageDialog(frame, "Mohon isi seluruh field", "Peringatan", JOptionPane.WARNING_MESSAGE);
                // }
            }
        });
        
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(new Color(190,170,140,255));
        kembaliButton.setForeground(Color.black);
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                ringkasanMahasiswaPanel.setVisible(false);
            }
        });

        // Menempelkan komponen ke panel
        ringkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0,145)));
        ringkasanMahasiswaPanel.add(titleLabel);
        ringkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0,20)));
        ringkasanMahasiswaPanel.add(pilihNPMLabel);
        ringkasanMahasiswaPanel.add(pilihNPM);
        ringkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0,20)));
        ringkasanMahasiswaPanel.add(lihatButton);
        ringkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0,10)));
        ringkasanMahasiswaPanel.add(kembaliButton);

        // Menempelkan panel ke frame
        frame.add(ringkasanMahasiswaPanel);
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    
}
