package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Setting panel
        JPanel irsPanel = new JPanel();
        irsPanel.setLayout(new BoxLayout(irsPanel, BoxLayout.Y_AXIS));
        irsPanel.setBackground(new Color(84,16,29,255));

        // Setting judul
        JLabel titleLabel = new JLabel("Tambah IRS");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);

        // Setting NPM dropdown
        Long[] daftarNPMmahasiswa = new Long[daftarMahasiswa.size()];
        int j = 0;
        for (Mahasiswa i : daftarMahasiswa) {
            daftarNPMmahasiswa[j++] = i.getNpm(); 
        }

        for (int m = 0; m < daftarNPMmahasiswa.length; m++) {
            for (int n = m+1; n < daftarNPMmahasiswa.length; n++) {
                long tmp = 0;
                if (daftarNPMmahasiswa[m] > daftarNPMmahasiswa[n]) {
                    tmp = daftarNPMmahasiswa[m];
                    daftarNPMmahasiswa[m] = daftarNPMmahasiswa[n];
                    daftarNPMmahasiswa[n] = tmp;
                }
            }
        }
        
        JLabel pilihNPMLabel = new JLabel("Pilih NPM");
        pilihNPMLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihNPMLabel.setForeground(Color.white);
        JComboBox<Long> pilihNPM = new JComboBox<Long>(daftarNPMmahasiswa);
        pilihNPM.setMaximumSize(new Dimension(200,23));

        // Setting matkul dropdown
        String[] daftarMatKul = new String[daftarMataKuliah.size()];
        int k = 0;
        for (MataKuliah l : daftarMataKuliah) {
            daftarMatKul[k++] = l.getNama();
        }
        
        for (int o = 0; o < daftarMatKul.length; o++) {
            for (int p = o+1; p < daftarMatKul.length; p++) {
                String tmp = "";
                if (daftarMatKul[o].compareTo(daftarMatKul[p]) > 0) {
                    tmp = daftarMatKul[o];
                    daftarMatKul[o] = daftarMatKul[p];
                    daftarMatKul[p] = tmp;
                }
            }
        }

        JLabel pilihMatkulLabel = new JLabel("Pilih Nama Matkul");
        pilihMatkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihMatkulLabel.setForeground(Color.white);
        JComboBox<String> pilihMatkul = new JComboBox<String>(daftarMatKul);
        pilihMatkul.setMaximumSize(new Dimension(200,23));

        // Setting button
        JButton tambahkanButton = new JButton("Tambahkan");
        tambahkanButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahkanButton.setBackground(new Color(130,170,190,255));
        tambahkanButton.setForeground(Color.black);
        tambahkanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                try {
                    MataKuliah mataKuliah = getMataKuliah((String)pilihMatkul.getSelectedItem(), daftarMataKuliah);
                    Mahasiswa mahasiswa = getMahasiswa((long)pilihNPM.getSelectedItem(), daftarMahasiswa);
                    String message = mahasiswa.addMatkul(mataKuliah);
                    JOptionPane.showMessageDialog(frame, message, "Add Matkul", JOptionPane.PLAIN_MESSAGE);
                } catch (Exception f) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh field", "Peringatan", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(new Color(190,170,140,255));
        kembaliButton.setForeground(Color.black);
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                irsPanel.setVisible(false);
            }
        });

        // Menempelkan komponen ke panel
        irsPanel.add(Box.createRigidArea(new Dimension(0,115)));
        irsPanel.add(titleLabel);
        irsPanel.add(Box.createRigidArea(new Dimension(0,20)));
        irsPanel.add(pilihNPMLabel);
        irsPanel.add(pilihNPM);
        irsPanel.add(Box.createRigidArea(new Dimension(0,10)));
        irsPanel.add(pilihMatkulLabel);
        irsPanel.add(pilihMatkul);
        irsPanel.add(Box.createRigidArea(new Dimension(0,20)));
        irsPanel.add(tambahkanButton);
        irsPanel.add(Box.createRigidArea(new Dimension(0,10)));
        irsPanel.add(kembaliButton);
        
        // Menempelkan panel ke frame
        frame.add(irsPanel);
    }

    
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    
}
