package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // Setting panel
        JPanel panelTambahMahasiswa = new JPanel();
        panelTambahMahasiswa.setLayout(new BoxLayout(panelTambahMahasiswa, BoxLayout.Y_AXIS));
        panelTambahMahasiswa.setBackground(new Color(84,16,29,255));
        
        // Setting judul
        JLabel titleLabel = new JLabel("Tambah Mahasiswa");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);
        
        // Setting textfield
        JLabel namaLabel = new JLabel("Nama:");
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaLabel.setForeground(Color.white);
        JTextField namaTextField = new JTextField();
        namaTextField.setMaximumSize(new Dimension(200,23));
        
        JLabel npmLabel = new JLabel("NPM:");
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        npmLabel.setForeground(Color.white);
        JTextField npmTextField = new JTextField();
        npmTextField.setMaximumSize(new Dimension(200,23));
        
        // Setting button
        JButton tambahkanButton = new JButton("Tambahkan");
        tambahkanButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahkanButton.setBackground(new Color(130,170,190,255));
        tambahkanButton.setForeground(Color.black);
        tambahkanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String nama = namaTextField.getText();
                long npm;
                
                try {
                    npm = Long.parseLong(npmTextField.getText());
                } catch (NumberFormatException f) {
                    npm = 0;
                }

                if (nama.equals("") || npm == 0) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh field", "Peringatan", JOptionPane.WARNING_MESSAGE);
                }
                
                else if (sudahTerdaftar(npm, daftarMahasiswa)) {
                    JOptionPane.showMessageDialog(frame, String.format("NPM %s sudah pernah ditambahkan sebelumnya", npm), "Error", JOptionPane.ERROR_MESSAGE);
                    npmTextField.setText("");
                    namaTextField.setText("");
                }
                
                else {
                    daftarMahasiswa.add(new Mahasiswa(namaTextField.getText(), Long.parseLong(npmTextField.getText())));
                    JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %s-%s berhasil ditambahkan", npm, nama), "Sukses", JOptionPane.INFORMATION_MESSAGE);
                    namaTextField.setText("");
                    npmTextField.setText("");
                }
            }
        });
        
        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(new Color(190,170,140,255));
        kembaliButton.setForeground(Color.black);
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelTambahMahasiswa.setVisible(false);
            }
        });

        // Menempelkan komponen ke panel
        panelTambahMahasiswa.add(Box.createRigidArea(new Dimension(0, 110)));
        panelTambahMahasiswa.add(titleLabel);
        panelTambahMahasiswa.add(Box.createRigidArea(new Dimension(0, 20)));
        panelTambahMahasiswa.add(namaLabel);
        panelTambahMahasiswa.add(namaTextField);
        panelTambahMahasiswa.add(Box.createRigidArea(new Dimension(0, 10)));
        panelTambahMahasiswa.add(npmLabel);
        panelTambahMahasiswa.add(npmTextField);
        panelTambahMahasiswa.add(Box.createRigidArea(new Dimension(0, 20)));
        panelTambahMahasiswa.add(tambahkanButton);
        panelTambahMahasiswa.add(Box.createRigidArea(new Dimension(0, 10)));
        panelTambahMahasiswa.add(kembaliButton);
        panelTambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Menempelkan panel ke frame
        frame.add(panelTambahMahasiswa);
    }
    
    private boolean sudahTerdaftar(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        for (Mahasiswa i : daftarMahasiswa) {
            if (npm == i.getNpm()) {
                return true;
            }
        }
        return false;
    }
}
