package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Setting judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setForeground(Color.white);
        
        // Setting panel
        JPanel panelHomeGUI = new JPanel();
        panelHomeGUI.setSize(1000, 1000);
        panelHomeGUI.setBackground(new Color(50,33,41,255));
        panelHomeGUI.setLayout(new BoxLayout(panelHomeGUI, BoxLayout.Y_AXIS));

        // Setting button
        JButton tambahMahasiswa = new JButton("Tambah Mahasiswa");
        tambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMahasiswa.setBackground(new Color(213,197,174,255));
        tambahMahasiswa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHomeGUI.setVisible(false);
            }
        });
        
        JButton tambahMatKul = new JButton("Tambah Mata Kuliah");
        tambahMatKul.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMatKul.setBackground(new Color(213,197,174,255));
        tambahMatKul.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHomeGUI.setVisible(false);
            }
        });
        
        JButton tambahIRS = new JButton("Tambah IRS");
        tambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahIRS.setBackground(new Color(213,197,174,255));
        tambahIRS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHomeGUI.setVisible(false);
            }
        });
        
        JButton hapusIRS = new JButton("Hapus IRS");
        hapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        hapusIRS.setBackground(new Color(213,197,174,255));
        hapusIRS.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHomeGUI.setVisible(false);
            }
        });
        
        JButton ringkasanMahasiswa = new JButton("Lihat Ringkasan Mahasiswa");
        ringkasanMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasanMahasiswa.setBackground(new Color(213,197,174,255));
        ringkasanMahasiswa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHomeGUI.setVisible(false);
            }
        });
        
        JButton ringkasanMatKul = new JButton("Lihat Ringkasan Mata Kuliah");
        ringkasanMatKul.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasanMatKul.setBackground(new Color(213,197,174,255));
        ringkasanMatKul.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHomeGUI.setVisible(false);
            }
        });

        // Menempelkan komponen ke panel
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,100)));
        panelHomeGUI.add(titleLabel, panelHomeGUI);
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,15)));
        panelHomeGUI.add(tambahMahasiswa);
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,10)));
        panelHomeGUI.add(tambahMatKul);
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,10)));
        panelHomeGUI.add(tambahIRS);
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,10)));
        panelHomeGUI.add(hapusIRS);
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,10)));
        panelHomeGUI.add(ringkasanMahasiswa);
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,10)));
        panelHomeGUI.add(ringkasanMatKul);
        panelHomeGUI.add(Box.createRigidArea(new Dimension(0,10)));
        
        
        // Menempelkan panel ke frame
        frame.add(panelHomeGUI);
    }
}
