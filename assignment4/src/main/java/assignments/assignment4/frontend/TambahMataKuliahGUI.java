package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // Setting panel
        JPanel tambahMatkulPanel = new JPanel();
        tambahMatkulPanel.setLayout(new BoxLayout(tambahMatkulPanel, BoxLayout.Y_AXIS));
        tambahMatkulPanel.setBackground(new Color(84,16,29,255));

        // Setting judul
        JLabel titleLabel = new JLabel("Tambah Mata Kuliah");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);

        // Settinf textfield
        JLabel kodeLabel = new JLabel("Kode Mata Kuliah:");
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kodeLabel.setForeground(Color.white);
        JTextField kodeTextField = new JTextField();
        kodeTextField.setMaximumSize(new Dimension(200,23));
        
        JLabel namaLabel = new JLabel("Nama Mata Kuliah:");
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaLabel.setForeground(Color.white);
        JTextField namaTextField = new JTextField();
        namaTextField.setMaximumSize(new Dimension(200,23));
        
        JLabel sksLabel = new JLabel("SKS:");
        sksLabel.setForeground(Color.white);
        sksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField sksTextField = new JTextField();
        sksTextField.setMaximumSize(new Dimension(200,23));
        
        JLabel kapasitasLabel = new JLabel("Kapasitas:");
        kapasitasLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasLabel.setForeground(Color.white);
        JTextField kapasitasTextField = new JTextField();
        kapasitasTextField.setMaximumSize(new Dimension(200,23));
        
        // Setting button
        JButton tambahkanButton = new JButton("Tambahkan");
        tambahkanButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahkanButton.setBackground(new Color(130,170,190,255));
        tambahkanButton.setForeground(Color.black);
        tambahkanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String kode = kodeTextField.getText();
                String nama = namaTextField.getText();
                int sks;
                int kapasitas;

                try {
                    sks = Integer.parseInt(sksTextField.getText());
                    kapasitas = Integer.parseInt(kapasitasTextField.getText());
                } catch (NumberFormatException f) {
                    sks = -1;
                    kapasitas = -1;
                }

                if (kode.equals("") || nama.equals("") || sks == -1 || kapasitas == -1) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh field", "Peringatan", JOptionPane.WARNING_MESSAGE);
                }
                
                else if (sudahTerdaftar(nama, daftarMataKuliah)) {
                    JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama), "Error", JOptionPane.ERROR_MESSAGE);
                    namaTextField.setText("");
                    kodeTextField.setText("");
                    sksTextField.setText("");
                    kapasitasTextField.setText("");
                }
                
                else {
                    daftarMataKuliah.add(new MataKuliah(kode, nama, sks, kapasitas));
                    JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s berhasil ditambahkan", nama), "Sukses", JOptionPane.INFORMATION_MESSAGE);
                    namaTextField.setText("");
                    kodeTextField.setText("");
                    sksTextField.setText("");
                    kapasitasTextField.setText("");
                }
            }
        });

        JButton kembaliButton = new JButton("Kembali");
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(new Color(190,170,140,255));
        kembaliButton.setForeground(Color.black);
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                tambahMatkulPanel.setVisible(false);
            }
        });

        // Menempelkan komponen ke panel
        tambahMatkulPanel.add(Box.createRigidArea(new Dimension(0,75)));
        tambahMatkulPanel.add(titleLabel);
        tambahMatkulPanel.add(Box.createRigidArea(new Dimension(0,20)));
        tambahMatkulPanel.add(kodeLabel);
        tambahMatkulPanel.add(kodeTextField);
        tambahMatkulPanel.add(Box.createRigidArea(new Dimension(0,10)));
        tambahMatkulPanel.add(namaLabel);
        tambahMatkulPanel.add(namaTextField);
        tambahMatkulPanel.add(Box.createRigidArea(new Dimension(0,10)));
        tambahMatkulPanel.add(sksLabel);
        tambahMatkulPanel.add(sksTextField);
        tambahMatkulPanel.add(Box.createRigidArea(new Dimension(0,10)));
        tambahMatkulPanel.add(kapasitasLabel);
        tambahMatkulPanel.add(kapasitasTextField);
        tambahMatkulPanel.add(Box.createRigidArea(new Dimension(0,20)));
        tambahMatkulPanel.add(tambahkanButton);
        tambahMatkulPanel.add(Box.createRigidArea(new Dimension(0,10)));
        tambahMatkulPanel.add(kembaliButton);

        // Menempelkan panel ke frame
        frame.add(tambahMatkulPanel);
    }
    
    private boolean sudahTerdaftar(String nama, ArrayList<MataKuliah> daftarMataKuliah) {
        for (MataKuliah i : daftarMataKuliah) {
            if (nama.equals(i.getNama())) {
                return true;
            }
        }
        return false;
    }
}
