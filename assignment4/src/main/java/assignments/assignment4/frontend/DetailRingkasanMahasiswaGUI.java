package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Setting panel
        JPanel detailRingkasanMahasiswaPanel = new JPanel();
        detailRingkasanMahasiswaPanel.setLayout(new BoxLayout(detailRingkasanMahasiswaPanel, BoxLayout.Y_AXIS));
        detailRingkasanMahasiswaPanel.setBackground(new Color(30,40,60,255));
        
        // Setting judul
        JLabel titleLabel = new JLabel("Detail Ringkasan Mahasiswa");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.white);
        
        // Setting data
        JLabel nama = new JLabel(String.format("Nama: %s", mahasiswa.getNama()));
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setFont(SistemAkademikGUI.fontGeneral);
        nama.setForeground(Color.white);
        
        JLabel npm = new JLabel(String.format("NPM: %s", mahasiswa.getNpm()));
        npm.setAlignmentX(Component.CENTER_ALIGNMENT);
        npm.setFont(SistemAkademikGUI.fontGeneral);
        npm.setForeground(Color.white);
        
        JLabel jurusan = new JLabel(String.format("Jurusan: %s", mahasiswa.getJurusan()));
        jurusan.setAlignmentX(Component.CENTER_ALIGNMENT);
        jurusan.setFont(SistemAkademikGUI.fontGeneral);
        jurusan.setForeground(Color.white);
        
        JLabel matkul = new JLabel("Daftar Mata Kuliah:");
        matkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        matkul.setFont(SistemAkademikGUI.fontGeneral);
        matkul.setForeground(Color.white);
        
        JPanel listMatkul = new JPanel();
        listMatkul.setBackground(new Color(30,40,60,255));
        listMatkul.setLayout(new BoxLayout(listMatkul, BoxLayout.Y_AXIS));
        JLabel matkulz;
        if (mahasiswa.getBanyakMatkul() != 0) {
            for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) {
                matkulz = new JLabel(String.format("%s. %s\n", i+1, mahasiswa.getMataKuliah()[i].getNama()));
                matkulz.setAlignmentX(Component.CENTER_ALIGNMENT);
                matkulz.setForeground(Color.white);
                listMatkul.add(matkulz);
                listMatkul.add(Box.createRigidArea(new Dimension(0, 10)));
            }
        }
        else {
            matkulz = new JLabel("Belum ada mata kuliah yang diambil.");
            matkulz.setAlignmentX(Component.CENTER_ALIGNMENT);
            matkulz.setForeground(Color.white);
            listMatkul.add(matkulz);
        }
        
        JLabel sks = new JLabel(String.format("Total SKS: %s", mahasiswa.getTotalSKS()));
        sks.setAlignmentX(Component.CENTER_ALIGNMENT);
        sks.setFont(SistemAkademikGUI.fontGeneral);
        sks.setForeground(Color.white);
        
        JLabel masalah = new JLabel(String.format("Hasil Pengecekan IRS:"));
        masalah.setAlignmentX(Component.CENTER_ALIGNMENT);
        masalah.setFont(SistemAkademikGUI.fontGeneral);
        masalah.setForeground(Color.white);
        
        JLabel probs;
        JPanel listMasalah = new JPanel();
        listMasalah.setLayout(new BoxLayout(listMasalah, BoxLayout.Y_AXIS));
        listMasalah.setBackground(new Color(30,40,60,255));
        mahasiswa.cekIRS();
        if (mahasiswa.getBanyakMasalahIRS() != 0) {
            for (int j = 0; j < mahasiswa.getBanyakMasalahIRS(); j++) {
                probs = new JLabel(String.format("%s. %s\n", j+1, mahasiswa.getMasalahIRS()[j]));
                probs.setAlignmentX(Component.CENTER_ALIGNMENT);
                probs.setForeground(Color.white);
                listMasalah.add(probs);
                listMasalah.add(Box.createRigidArea(new Dimension(0, 10)));
            }
        }
        else {
            probs = new JLabel("IRS tidak bermasalah.");
            probs.setForeground(Color.white);
            probs.setAlignmentX(Component.CENTER_ALIGNMENT);
            listMasalah.add(probs);
        }

        // Setting button
        JButton selesaiButton = new JButton("Selesai");
        selesaiButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        selesaiButton.setBackground(new Color(190,170,140,255));
        selesaiButton.setForeground(Color.black);
        selesaiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                detailRingkasanMahasiswaPanel.setVisible(false);
            }
        });
        
        // Menempelkan komponen ke panel
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 70)));
        detailRingkasanMahasiswaPanel.add(titleLabel);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        detailRingkasanMahasiswaPanel.add(nama);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMahasiswaPanel.add(npm);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMahasiswaPanel.add(jurusan);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMahasiswaPanel.add(matkul);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMahasiswaPanel.add(listMatkul);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMahasiswaPanel.add(sks);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMahasiswaPanel.add(masalah);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        detailRingkasanMahasiswaPanel.add(listMasalah);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        detailRingkasanMahasiswaPanel.add(selesaiButton);
        detailRingkasanMahasiswaPanel.add(Box.createRigidArea(new Dimension(0, 100)));

        // Menempelkan panel ke frame
        frame.add(detailRingkasanMahasiswaPanel);
    }
}
