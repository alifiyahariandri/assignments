package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {

    public static boolean validate(long npm) {
        String npmStr = Long.toString(npm);
        String kodeJurusan = "01 02 03 11 12";

        // validasi banyak digit
        if (npmStr.length() != 14) {
            return false;
        }

        // validasi tahun masuk
        else if (Integer.parseInt(npmStr.substring(0,2)) < 10) {
            return false;
        }

        // validasi kode jurusan 
        else if (!kodeJurusan.contains(npmStr.substring(2,4))) {
            return false;
        }

        // validasi tahun lahir
        else if (Integer.parseInt("20" + npmStr.substring(0,2)) - Integer.parseInt(npmStr.substring(8,12)) < 15) {
            return false;
        }

        else {
            // menghitung kode npm
            int kodeNPM = kodeNPM(npmStr.substring(0,13));
            while (kodeNPM >= 10) {
                String kodeNPMstr = Integer.toString(kodeNPM);
                kodeNPM = 0;
                for (int i = 0; i < kodeNPMstr.length(); i++) {
                    kodeNPM += Integer.parseInt(kodeNPMstr.substring(i, i+1));
                }
            }

            // validasi kode npm
            if (kodeNPM == Integer.parseInt(npmStr.substring(13))) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public static int kodeNPM(String npmStr) {
        if (npmStr.length() == 1) {
            return Integer.parseInt(npmStr);
        }
        else {
            return Integer.parseInt(npmStr.substring(0,1))*Integer.parseInt(npmStr.substring(npmStr.length()-1, npmStr.length())) + kodeNPM(npmStr.substring(1, npmStr.length()-1));
        }
    }


    public static String extract(long npm) {
        String npmStr = Long.toString(npm);
        String jurusan = jurusan(npmStr.substring(2,4));
        
        return "Tahun masuk: 20" + npmStr.substring(0, 2) + "\nJurusan: " + jurusan + "\nTanggal Lahir: " + npmStr.substring(4, 6) + "-" + npmStr.substring(6, 8) + "-" + npmStr.substring(8, 12);
    }

    public static String jurusan(String kodeJurusan) {
        if (kodeJurusan.equals("01")) {
            return "Ilmu Komputer";
        }
        else if (kodeJurusan.equals("02")) {
            return "Sistem Informasi";
        }
        else if (kodeJurusan.equals("03")) {
            return "Teknologi Informasi";
        }
        else if (kodeJurusan.equals("11")) {
            return "Teknik Telekomunikasi";
        }
        else {
            return "Teknik Elektro";
        }
        
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            
            boolean valid = validate(npm);
            
            if (valid) {
                System.out.println(extract(npm));
            }
            else {
                System.out.println("NPM tidak valid!");
            }
        }
        input.close();
    }
}