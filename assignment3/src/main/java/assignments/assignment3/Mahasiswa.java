package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private int totalMataKuliah;
    private long npm;
    private String tanggalLahir;
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        this.nama = nama;
        this.npm = npm;
        setTipe();
    }

    // checker
    public boolean menyapaDosenMatkulSama(ElemenFasilkom dosen) {
        for (int j = 0; j < this.totalMataKuliah; j++) {
            if (this.daftarMataKuliah[j].getDosen() == dosen) {
                return true;
            }
        }            
        return false;
    }
    
    public boolean sudahMengambil(MataKuliah mataKuliah)  {
        for (int i = 0; i < this.totalMataKuliah; i++) {
            if (this.daftarMataKuliah[i] == mataKuliah) {
                return true;
            }
        }
        return false;
    }

    // getter
    public long getNPM() {
        return this.npm;
    }

    public int getTotalMataKuliah() {
        return this.totalMataKuliah;
    }

    public MataKuliah[] getDaftarMataKuliah() {
        return this.daftarMataKuliah;
    }

    public void addMatkul(MataKuliah mataKuliah) {
        if (this.sudahMengambil(mataKuliah)) {
            System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya", mataKuliah.getNama()));
        }
        else if (mataKuliah.sudahPenuh()) {
            System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya", mataKuliah.getNama()));
        }
        else {
            mataKuliah.addMahasiswa(this);
            this.daftarMataKuliah[totalMataKuliah++] = mataKuliah;
            System.out.println(String.format("%s berhasil menambahkan mata kuliah %s", getNama(), mataKuliah.getNama()));
        }

    }

    public void dropMatkul(MataKuliah mataKuliah) {
        if (!this.sudahMengambil(mataKuliah)) {
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil", mataKuliah.getNama()));
        }
        else {
            mataKuliah.dropMahasiswa(this);
            int indeks = 0;
            for (int i = 0; i < 10; i++) {
                if (this.daftarMataKuliah[i] == mataKuliah) {
                    indeks = i;
                    break;
                }
            }

            for (int j = indeks; j < 10; j++) {
                if (j == 9) {
                    this.daftarMataKuliah[j] = null;
                }
                else {
                    this.daftarMataKuliah[j] = this.daftarMataKuliah[j + 1];
                }
            }
            totalMataKuliah--;
            System.out.println(String.format("%s berhasil drop mata kuliah %s", getNama(), mataKuliah.getNama()));
        }
    }

    public String extractTanggalLahir(long npm) {
        String npmStr = Long.toString(npm);
        int tanggal = Integer.parseInt(npmStr.substring(4,6));
        int bulan = Integer.parseInt(npmStr.substring(6,8));
        int tahun = Integer.parseInt(npmStr.substring(8,12));

        this.tanggalLahir = String.format("%s-%s-%s", tanggal, bulan, tahun);
        return this.tanggalLahir;
    }

    public String extractJurusan(long npm) {
        String npmStr = Long.toString(npm);
        String kode = npmStr.substring(2,4);

        switch (kode) {
            case "01" : this.jurusan = "Ilmu Komputer"; break;
            case "02" : this.jurusan = "Sistem Informasi"; break;
            default : this.jurusan =  "";
        }

        return this.jurusan;
    }

    public void setTipe() {
        this.tipe = "Mahasiswa";
    }

}