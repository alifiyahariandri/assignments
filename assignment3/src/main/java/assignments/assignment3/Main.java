package assignments.assignment3;
import java.util.Scanner;

public class Main {
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalMataKuliah = 0;
    private static int totalElemenFasilkom = 0; 

    // method pembuat objek
    private static void addMahasiswa(String nama, long npm) {
        Mahasiswa mhs = new Mahasiswa(nama, npm);
        daftarElemenFasilkom[totalElemenFasilkom++] = mhs;
        System.out.println(String.format("%s berhasil ditambahkan", mhs.getNama()));
    }
    
    private static void addDosen(String nama) {
        Dosen dsn = new Dosen(nama);
        daftarElemenFasilkom[totalElemenFasilkom++] = dsn;
        System.out.println(String.format("%s berhasil ditambahkan", dsn.getNama()));
    }
    
    private static void addElemenKantin(String nama) {
        ElemenKantin elkan = new ElemenKantin(nama);
        daftarElemenFasilkom[totalElemenFasilkom++] = elkan;
        System.out.println(String.format("%s berhasil ditambahkan", elkan.getNama()));
    }

    private static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom elemen = getElemenFasilkom(objek);

        if (elemen.getTipe().equals("ElemenKantin")) {((ElemenKantin)elemen).setMakanan(namaMakanan, harga);}
        else {System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin", elemen.getNama()));}
    }

    private static void createMatkul(String nama, int kapasitas) {
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        daftarMataKuliah[totalMataKuliah++] = matkul;
        System.out.println(String.format("%s berhasil ditambahkan dengan kapasitas %s", matkul.getNama(), matkul.getKapasitas()));
    }

    // getter    
    private static ElemenFasilkom getElemenFasilkom(String nama) {
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(nama)) {return daftarElemenFasilkom[i];}
        }
        return null;
    }
    
    private static MataKuliah getMataKuliah(String namaMataKuliah) {
        for (int i = 0; i < totalMataKuliah; i++) {
            if (daftarMataKuliah[i].getNama().equals(namaMataKuliah)) {return daftarMataKuliah[i];}
        }
        return null;
    }     
    
    // aktivitas
    private static void menyapa(String objek1, String objek2) {
        ElemenFasilkom objek1found = getElemenFasilkom(objek1);
        ElemenFasilkom objek2found = getElemenFasilkom(objek2);

        if (objek1found == objek2found) {System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");}
        else {objek1found.menyapa(objek2found);}
    }
    
    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom elemen1 = getElemenFasilkom(objek1);
        ElemenFasilkom elemen2 = getElemenFasilkom(objek2);
        
        if (elemen2.getTipe().equals("ElemenKantin")) {
            if (elemen1 == elemen2) {System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");}
            else {elemen1.membeliMakanan(elemen1, elemen2, namaMakanan);}
        }
        
        else {System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");}
    }
    
    private static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElemenFasilkom(objek);
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
     
        if (elemen.getTipe().equals("Mahasiswa")) {((Mahasiswa)elemen).addMatkul(mataKuliah);}
        else {System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");}
    }

    private static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElemenFasilkom(objek);
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
     
        if (elemen.getTipe().equals("Mahasiswa")) {((Mahasiswa)elemen).dropMatkul(mataKuliah);}
        else {System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");}
    }

    private static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElemenFasilkom(objek);
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
     
        if (elemen.getTipe().equals("Dosen")) {((Dosen)elemen).mengajarMataKuliah(mataKuliah);}
        else {System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");}
    }

    private static void berhentiMengajar(String objek) {
        ElemenFasilkom elemen = getElemenFasilkom(objek);
     
        if (elemen.getTipe().equals("Dosen")) {((Dosen)elemen).dropMataKuliah();}
        else {System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");}

    }

    // ringkasan
    private static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom elemen = getElemenFasilkom(objek);
     
        if (elemen.getTipe().equals("Mahasiswa")) {
            System.out.println("Nama: " + elemen.getNama());
            System.out.println("Tanggal lahir: " + ((Mahasiswa)elemen).extractTanggalLahir(((Mahasiswa)elemen).getNPM()));
            System.out.println("Jurusan: " + ((Mahasiswa)elemen).extractJurusan(((Mahasiswa)elemen).getNPM()));
            System.out.println("Daftar Mata Kuliah:");

            if (((Mahasiswa)elemen).getTotalMataKuliah() == 0) {System.out.println("Belum ada mata kuliah yang diambil");}
            else {
                int k = 1;
                for (int i = 0; i < ((Mahasiswa)elemen).getTotalMataKuliah(); i++) {
                    System.out.println(k + ". " + ((Mahasiswa)elemen).getDaftarMataKuliah()[i].getNama());
                    k++;
                }
            }
        }
        
        else {System.out.println(String.format("[DITOLAK] %s bukan merupakan seorang mahasiswa", elemen.getNama()));}
        
    }

    private static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

        System.out.println("Nama mata kuliah: " + mataKuliah.getNama());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.print("Dosen pengajar: ");
        if (mataKuliah.getDosen() == null) {System.out.println("Belum ada");}
        else {System.out.println(mataKuliah.getDosen());} 
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");

        if (mataKuliah.getJumlahMahasiswa() == 0) System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        else {
            int k = 1;
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
                System.out.println(k + ". " + mataKuliah.getDaftarMahasiswa()[i].getNama());
                k++;
            }
        }
    }

    // reset & akumulator keramahan
    private static void nextDay() {
        for (int i = 0; i < totalElemenFasilkom; i++) {
            ElemenFasilkom elemen = daftarElemenFasilkom[i];
            double setengah = (totalElemenFasilkom-1.0)/2;
            
            if (elemen.getFriendship() > 100) {elemen.setFriendship(100);}
            else if (elemen.getFriendship() < 0) {elemen.setFriendship(0);}

            if (elemen.getJumlahMenyapa() >= setengah) {elemen.setFriendship(elemen.getFriendship()+10);}
            else {elemen.setFriendship(elemen.getFriendship()-5);}

            if (elemen.getFriendship() > 100) {elemen.setFriendship(100);}
            else if (elemen.getFriendship() < 0) {elemen.setFriendship(0);}

            elemen.resetMenyapa();
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    private static void friendshipRanking() {
        ElemenFasilkom[] sorted = new ElemenFasilkom[totalElemenFasilkom];

        for (int i = 0; i < totalElemenFasilkom; i++) {
            sorted[i] = daftarElemenFasilkom[i];
        }

        for (int i = 0; i < sorted.length; i++) {
            for (int j = i+1; j < sorted.length; j++) {
                ElemenFasilkom tmp = null;
                if (sorted[i].getFriendship() < sorted[j].getFriendship()) {
                    tmp = sorted[i];
                    sorted[i] = sorted[j];
                    sorted[j] = tmp;
                }
                else if (sorted[i].getFriendship() == sorted[j].getFriendship()) {
                    if (sorted[i].getNama().compareTo(sorted[j].getNama()) > 0) {
                        tmp = sorted[i];
                        sorted[i] = sorted[j];
                        sorted[j] = tmp;
                    }
                }
                else continue;
            }
        }

        int k = 1;
        for (ElemenFasilkom elemen : sorted) {
            System.out.println(k + ". " + elemen + "(" + elemen.getFriendship() + ")");
            k++;
        }
    }

    private static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
        input.close();
    }
}