package assignments.assignment3;

class Dosen extends ElemenFasilkom {
    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        this.nama = nama;
        setTipe();
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if (this.mataKuliah != null) {
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", getNama(), this.mataKuliah.getNama()));
        }
        else if (mataKuliah.getDosen() != null) {
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar", mataKuliah.getNama()));
        }
        else {
            mataKuliah.addDosen(this);
            this.mataKuliah = mataKuliah;
            System.out.println(String.format("%s mengajar mata kuliah %s", getNama(), this.mataKuliah.getNama()));
        }
    }

    public void dropMataKuliah() {
        if (this.mataKuliah == null) {
            System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun", getNama()));
        }
        else {
            System.out.println(String.format("%s berhenti mengajar %s", getNama(), this.mataKuliah.getNama()));
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }

    public void setTipe() {
        this.tipe = "Dosen";
    }

}