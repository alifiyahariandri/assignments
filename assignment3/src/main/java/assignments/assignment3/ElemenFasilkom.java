package assignments.assignment3;

abstract class ElemenFasilkom {
    protected String tipe;
    protected String nama;
    private int friendship;
    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];
    private int jumlahMenyapa;

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        if (sudahMenyapa(elemenFasilkom)) {
            System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini", this.getNama(), elemenFasilkom.getNama()));
        }
        else {
            System.out.println(String.format("%s menyapa dengan %s", this.getNama(), elemenFasilkom.getNama()));
            this.telahMenyapa[this.jumlahMenyapa++] = elemenFasilkom;
            elemenFasilkom.telahMenyapa[elemenFasilkom.jumlahMenyapa++] = this;

            // mengatur friendship mahasiswa yang menyapa dosen di matkul yg terhubung
            if (this.getTipe().equals("Mahasiswa") && elemenFasilkom.getTipe().equals("Dosen")) {
                if (((Mahasiswa)this).menyapaDosenMatkulSama(elemenFasilkom)) {
                    this.setFriendship(this.getFriendship()+2);
                    elemenFasilkom.setFriendship(elemenFasilkom.getFriendship()+2);
                }           
            }

            else if (this.getTipe().equals("Dosen") && elemenFasilkom.getTipe().equals("Mahasiswa")) {
                if (((Mahasiswa)elemenFasilkom).menyapaDosenMatkulSama(this)) {
                    this.setFriendship(this.getFriendship()+2);
                    elemenFasilkom.setFriendship(elemenFasilkom.getFriendship()+2);
                }           
            }
        }
    }

    public boolean sudahMenyapa(ElemenFasilkom seseorang) {
        boolean sudahMenyapa = false;
        for (int i = 0; i < jumlahMenyapa; i++) {
            if (this.telahMenyapa[i] == seseorang) {
                sudahMenyapa = true;
            }
        }
        return sudahMenyapa;
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        Makanan makanan = ((ElemenKantin)penjual).getMakanan(namaMakanan);

        if (makanan != null) {
            System.out.println(String.format("%s berhasil membeli %s seharga %s", pembeli.getNama(), makanan.getNama(), makanan.getHarga()));
            penjual.setFriendship(penjual.getFriendship()+1);
            pembeli.setFriendship(pembeli.getFriendship()+1);
        }
        else {
            System.out.println(String.format("[DITOLAK] %s tidak menjual %s", penjual.getNama(), namaMakanan));
        }
    }

    // getter
    public String getNama() {
        return this.nama;
    }

    public String getTipe() {
        return this.tipe;
    }

    public ElemenFasilkom[] getTelahMenyapa() {
        return this.telahMenyapa;
    }

    public int getFriendship() {
        return this.friendship;
    }

    public int getJumlahMenyapa() {
        return this.jumlahMenyapa;
    }

    // setter
    public void setFriendship(int angka) {
        this.friendship = angka;
    }

    public abstract void setTipe();

    public void resetMenyapa() {
        for (int i = 0; i < jumlahMenyapa; i++) {
            telahMenyapa[i] = null;
        }
        this.jumlahMenyapa = 0;
    }

    public String toString() {
        return this.nama;
    }
}