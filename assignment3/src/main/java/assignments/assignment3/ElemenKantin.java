package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    private Makanan[] daftarMakanan = new Makanan[10];
    private int totalMakanan;

    public ElemenKantin(String nama) {
        this.nama = nama;
        setTipe();
    }

    public void setMakanan(String nama, long harga) {
        Makanan makanan = new Makanan(nama, harga);

        if (sudahTerdaftar(makanan)) {
            System.out.println(String.format("[DITOLAK] %s sudah pernah terdaftar", makanan.getNama()));
        }
        else {
            this.daftarMakanan[totalMakanan++] = makanan;
            System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %s", getNama(), makanan.getNama(), makanan.getHarga()));
        }
    }

    public Makanan getMakanan(String nama) {
        for (int i = 0; i < totalMakanan; i++) {
            if (this.daftarMakanan[i].getNama().equals(nama)) {
                return this.daftarMakanan[i];
            }
        }
        return null;
    }

    public boolean sudahTerdaftar(Makanan makanan) {
        for (int i = 0; i < totalMakanan; i++) {
            if (this.daftarMakanan[i].getNama().equals(makanan.getNama())) {
                return true;
            }
        }
        return false;
    }

    public void setTipe() {
        this.tipe = "ElemenKantin";
    }



}