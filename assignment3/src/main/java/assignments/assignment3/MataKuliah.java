package assignments.assignment3;

class MataKuliah {
    private String nama;
    private int kapasitas;
    private Dosen dosen;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa;

    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    // getter
    public String getNama() {
        return this.nama;
    }

    public int getJumlahMahasiswa() {
        return this.jumlahMahasiswa;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    public Dosen getDosen() {
        return this.dosen;
    }

    // checker
    public boolean sudahPenuh() {
        if (jumlahMahasiswa == kapasitas) {
            return true;
        }
        else return false;
    }

    // setter
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.daftarMahasiswa[jumlahMahasiswa++] = mahasiswa;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int indeks = 0;
        for (int i = 0; i < kapasitas; i++) {
            if (this.daftarMahasiswa[i] == mahasiswa) {
                indeks = i;
                break;
            }
        }

        for (int j = indeks; j < kapasitas; j++) {
            if (j == kapasitas-1) {
                this.daftarMahasiswa[j] = null;
            }
            else {
                this.daftarMahasiswa[j] = this.daftarMahasiswa[j + 1];
            }
        }
        jumlahMahasiswa--;
    }

    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public void dropDosen() {
        this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }
}